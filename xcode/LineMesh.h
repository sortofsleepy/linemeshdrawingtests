//
//  LineMesh.h
//  MeshDrawingTests
//
//  Created by Joseph Chow on 8/27/15.
//
//

#ifndef __MeshDrawingTests__LineMesh__
#define __MeshDrawingTests__LineMesh__

#include "cinder/GeomIo.h"
#include "cinder/Vector.h"

using namespace ci;
using namespace geom;

class LineMesh : public Source {
    
    //! stroke width of the line
    int lineWidth = 1;
    
    //! line color
    ci::vec4 color;
    
    //default drawing style
    geom::Primitive drawingStyle;

    std::vector<float> positions;
public:
    LineMesh();
    
    //! sets the stroke of the line
    LineMesh& setLineWidth(int width);
    
    //! sets the color for the line
    LineMesh& setColor(ci::vec4 color);
    
    //! creates a basic line with a start point and a end point
    LineMesh& makeLine(ci::vec3 start, ci::vec3 end);
    
    //! creates a line using a BSpline
    LineMesh& makeLine(const ci::BSpline3f spline, int subdivisions );
    
    //! sets the drawing style of the line
    LineMesh& setDrawingStyle(geom::Primitive drawingStyle);
    
    LineMesh&		subdivisions( int subdivs );
    
    size_t		getNumVertices() const override;
    size_t		getNumIndices() const override { return 0; }
    Primitive	getPrimitive() const override { return drawingStyle;}
    uint8_t		getAttribDims( Attrib attr ) const override;
    AttribSet	getAvailableAttribs() const override;
    void		loadInto( Target *target, const AttribSet &requestedAttribs ) const override;
    LineMesh*		clone() const override { return new LineMesh( *this ); }
    
private:

    
    int			mRequestedSubdivisions, mNumSubdivisions;
    size_t		mNumVertices;
};


#endif /* defined(__MeshDrawingTests__LineMesh__) */
