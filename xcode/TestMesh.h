//
//  TestMesh.h
//  MeshDrawingTests
//
//  Created by Joseph Chow on 8/29/15.
//
//

#ifndef __MeshDrawingTests__TestMesh__
#define __MeshDrawingTests__TestMesh__

#include "cinder/gl/VboMesh.h"
#include "cinder/Rand.h"
#include "LineMeshGroup.h"

class TestMesh {
    
    ci::gl::VboMeshRef mesh;
    ci::gl::GlslProgRef renderShader;
    ci::gl::BatchRef batch;
    
    std::vector<ci::vec3> points;
    
    LineMeshGroup group;
    
    bool drawCube = false;
public:
    TestMesh();
    void setup();
    void draw();
    void placeLines();
    std::vector<ci::vec3> getPoints();
};

#endif /* defined(__MeshDrawingTests__TestMesh__) */
