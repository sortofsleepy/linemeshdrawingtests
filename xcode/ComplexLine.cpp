//
//  ComplexLine.cpp
//  MeshDrawingTests
//
//  Created by Joseph Chow on 8/28/15.
//
//

#include "ComplexLine.h"
// ComplexLine
ComplexLine::ComplexLine():mNumVertices(3),
drawingStyle(Primitive::TRIANGLE_FAN),
color(vec4(255,255,0,1)){
    
}

ComplexLine& ComplexLine::setLineWidth(int width){
    this->lineWidth = width;
    return *this;
}

ComplexLine& ComplexLine::setColor(ci::vec4 color){
    this->color = color;
    return *this;
}

size_t ComplexLine::getNumVertices() const
{
    return mNumVertices;
}

ComplexLine& ComplexLine::makeLine(ci::vec3 start, ci::vec3 end){
    positions.push_back(start);
    positions.push_back(end);
    
    return *this;
}

ComplexLine& ComplexLine::makeLine(const ci::BSpline3f spline, int subdivisions ){
    
    const float tInc = 1.0f / ( subdivisions - 1 );
    
    for( size_t i = 0; i < (size_t)subdivisions; ++i ) {
        auto pos = spline.getPosition( i * tInc );
        positions.push_back(pos);
    }
    
    return *this;
}

uint8_t	ComplexLine::getAttribDims( Attrib attr ) const
{
    switch( attr ) {
        case Attrib::POSITION: return 2;
        case Attrib::NORMAL: return 3;
        case Attrib::TEX_COORD_0: return 2;
        case Attrib::COLOR: return 4;
        default:
            return 0;
    }
}

AttribSet ComplexLine::getAvailableAttribs() const
{
    return { Attrib::POSITION, Attrib::NORMAL, Attrib::TEX_COORD_0,Attrib::COLOR };
}

void ComplexLine::loadInto( Target *target, const AttribSet &requestedAttribs ) const
{
    std::vector<vec2> texCoords;
    std::vector<vec3> vertices;
    std::vector<vec3> normals;
    
    std::vector<ci::vec3> finalPositions;
    
    //! setup the stroke size
    std::vector<float> strokeSize;
    strokeSize.push_back(lineWidth);
    
    //setup the color
    std::vector<vec4> vertexColors( positions.size(), color );
    
    //if we only have two points in the line, create a new point halfway between the start and the end
    
    if(positions.size() < 3){
        vec3 thisPoint = positions[0];
        vec3 nextPoint = positions[1];
        
        float halfX = ((thisPoint.x + nextPoint.x) / 2.0f) + random();
        float halfY = (thisPoint.y + nextPoint.y) / 2.0f + random();
        float halfZ = (thisPoint.z + nextPoint.z) / 2.0f + random();
        
        finalPositions.push_back(vec3(halfX,halfY,halfZ));
    }
    
    
    for(unsigned int i = 1; i < positions.size(); i++){
        vec3 thisPoint = positions[i - 1];
        vec3 nextPoint = positions[i];
        
        vec3 direction = (nextPoint - thisPoint);
        float distance = glm::length(direction);
        
        vec3 unitDirection = glm::normalize(direction);
        
        vec3 toTheLeft = xoio::MathUtils::getRotated(unitDirection, -90, vec3(0,0,1));
        vec3 toTheRight = xoio::MathUtils::getRotated(unitDirection, 90, vec3(0,0,1));

        float thickness = xoio::MathUtils::map(distance, 0, 60, 20, 2);
        
        vec3 leftPoint = thisPoint + toTheLeft * thickness;
        vec3 rightPoint = thisPoint + toTheRight * thickness;
        
        finalPositions.push_back(leftPoint);
        finalPositions.push_back(rightPoint);

    }
    
   
    target->copyAttrib( Attrib::POSITION, 3, 0, (const float*)finalPositions.data(), mNumVertices );
    target->copyAttrib( Attrib::COLOR, 4, 0, (const float*)vertexColors.data(), mNumVertices );

    target->copyAttrib(Attrib::CUSTOM_0, 1, 0, (const float*)strokeSize.data(), mNumVertices);
    
    target->copyAttrib( Attrib::NORMAL, 3, 0, (const float*)normals.data(), mNumVertices );
    target->copyAttrib( Attrib::TEX_COORD_0, 2, 0, (const float*)texCoords.data(), mNumVertices );
}

