//
//  TestMesh.cpp
//  MeshDrawingTests
//
//  Created by Joseph Chow on 8/29/15.
//
//

#include "TestMesh.h"
using namespace ci;
using namespace std;

TestMesh::TestMesh(){}

void TestMesh::setup(){
    

    int n = 1000;
    int n2 = n / 2;
    
    int size = 60000;
    for(int i = 0; i < size; ++i){
        float x = Rand::randFloat() * n - n2;
        float y = Rand::randFloat() * n - n2;
        float z = Rand::randFloat() * n - n2;
        points.push_back(vec3(x,y,z));
    }
    
    gl::VboMesh::Layout layout;
    layout.usage( GL_STATIC_DRAW ).attrib( geom::POSITION, 3 );
    
    mesh = gl::VboMesh::create(points.size(), GL_POINTS ,{layout});
    mesh->bufferAttrib(geom::POSITION, points.size() * sizeof(vec3), points.data());
    renderShader = gl::getStockShader(gl::ShaderDef().color());
    
    //create the lines to connect points in the mesh
    placeLines();
    
    //create batch for debug drawing
    batch = gl::Batch::create(mesh, renderShader);
    
}

void TestMesh::draw(){
    if(drawCube){
        gl::color(255, 0, 0);
        batch->draw();
    }else{
        group.draw();
    }
}

void TestMesh::placeLines(){
    
    for(int i = 0; i < points.size(); i+=2){
        ci::vec3 thisPoint = points.at(i);
        ci::vec3 nextPoint = points.at(i++);
        
        group.test(geom::Circle().radius(0.8));
        //group.addLine(ComplexLine().makeLine(thisPoint, nextPoint));
    }
    
    //prepare all the meshes.
    group.prepare();
    
}

std::vector<ci::vec3> TestMesh::getPoints(){
    return points;
}