//
//  LineMesh.cpp
//  MeshDrawingTests
//
//  Created by Joseph Chow on 8/27/15.
//
//

#include "LineMesh.h"
// LineMesh
LineMesh::LineMesh():mNumVertices(2),
drawingStyle(Primitive::LINE_STRIP),
color(vec4(255,0,0,1)){
 
}

LineMesh& LineMesh::setLineWidth(int width){
    this->lineWidth = width;
    return *this;
}

LineMesh& LineMesh::setColor(ci::vec4 color){
    this->color = color;
    return *this;
}

size_t LineMesh::getNumVertices() const
{
    return mNumVertices;
}

LineMesh& LineMesh::makeLine(ci::vec3 start, ci::vec3 end){
    positions.push_back(start.x);
    positions.push_back(start.y);
    positions.push_back(start.z);
    
    positions.push_back(end.x);
    positions.push_back(end.y);
    positions.push_back(end.z);
    
    return *this;
}

LineMesh& LineMesh::makeLine(const ci::BSpline3f spline, int subdivisions ){
    
    const float tInc = 1.0f / ( subdivisions - 1 );
    
    for( size_t i = 0; i < (size_t)subdivisions; ++i ) {
        auto pos = spline.getPosition( i * tInc );
        positions.push_back( pos.x );
        positions.push_back( pos.y );
        positions.push_back( pos.z );
    }
    
    return *this;
}

uint8_t	LineMesh::getAttribDims( Attrib attr ) const
{
    switch( attr ) {
        case Attrib::POSITION: return 2;
        case Attrib::NORMAL: return 3;
        case Attrib::TEX_COORD_0: return 2;
        case Attrib::COLOR: return 4;
        default:
            return 0;
    }
}

AttribSet LineMesh::getAvailableAttribs() const
{
    return { Attrib::POSITION, Attrib::NORMAL, Attrib::TEX_COORD_0,Attrib::COLOR };
}

void LineMesh::loadInto( Target *target, const AttribSet &requestedAttribs ) const
{
    std::vector<vec2> texCoords;
    std::vector<vec3> vertices;
    std::vector<vec3> normals;
    
    //! setup the stroke size
    std::vector<float> strokeSize;
    strokeSize.push_back(lineWidth);

    //setup the color
    std::vector<vec4> vertexColors( positions.size(), color );

    
    target->copyAttrib( Attrib::POSITION, 3, 0, (const float*)positions.data(), mNumVertices );
    target->copyAttrib( Attrib::COLOR, 4, 0, (const float*)vertexColors.data(), mNumVertices );
    //target->copyAttrib(Attrib::CUSTOM_0, 1, 0, (const float*)strokeSize.data(), mNumVertices);
    
   // target->copyAttrib( Attrib::NORMAL, 3, 0, (const float*)normals.data(), mNumVertices );
   // target->copyAttrib( Attrib::TEX_COORD_0, 2, 0, (const float*)texCoords.data(), mNumVertices );
}

