//
//  MathUtils.h
//  MeshDrawingTests
//
//  various math utils extracted from areas or added myself.
//  Created by Joseph Chow on 8/27/15.
//
//

#ifndef MeshDrawingTests_Utils_h
#define MeshDrawingTests_Utils_h

#include "cinder/Vector.h"

namespace xoio {
    
    namespace MathUtils {
        /// \brief Return a new 'ofVec3f' that is the result of rotating this
        /// vector by 'angle' radians around the given axis.
        ///
        /// ~~~~{.cpp}
        /// ofVec3f v1(1, 0, 0);
        /// // rotate v1 around the z axis
        /// ofVec3f v2 = v1.getRotated(PI/4, ofVec3f(0, 0, 1)); // v2 is (√2, √2, 0)
        /// // rotate v1 around the y axis
        /// ofVec3f v3 = v1.getRotated(PI/4, ofVec3f(0, 1, 0)); // v3 is (√2, 0, √2)
        /// ~~~~
        static ci::vec3 getRotated(ci::vec3 value, float angle, const ci::vec3 &axis) {
            ci::vec3 ax = glm::normalize(axis);
            float a = (float)(angle * 0.0174533);
            float sina = sin(a);
            float cosa = cos(a);
            float cosb = 1.0f - cosa;
            
            
            float xval =  value.x * (ax.x * ax.x * cosb + cosa) + value.y * (ax.x * ax.y * cosb - ax.z * sina) + value.z*(ax.x * ax.z * cosb + ax.y * sina);
            
            float yval =  value.x*(ax.y*ax.x*cosb + ax.z*sina)
            + value.y *(ax.y*ax.y*cosb + cosa)
            + value.z *(ax.y*ax.z*cosb - ax.x*sina);
            
            float zval = value.x * (ax.z*ax.x*cosb - ax.y*sina)
            + value.y * (ax.z*ax.y*cosb + ax.x*sina)
            + value.z * (ax.z*ax.z*cosb + cosa);
            
            return ci::vec3(xval,yval,zval);
        };
        
        
        /**
         * Re-maps a number from one range to another.
         * @param val value to re-map
         * @param startMin start min
         * @param startMax start max
         * @param endMin end min
         * @param endMax end max
         * @returns {*}
         */
        static float map(float val, float startMin, float startMax, float endMin, float endMax){
            return ( val - startMin) / (startMax - startMin) * ( startMax - startMin) + startMin;
        }

    }
}

#endif
