//
//  LineMeshGroup.cpp
//  MeshDrawingTests
//
//  Created by Joseph Chow on 8/27/15.
//
//

#include "LineMeshGroup.h"
using namespace ci;
using namespace std;

LineMeshGroup::LineMeshGroup(){}

void LineMeshGroup::addLine(ci::vec3 start, ci::vec3 end){
    addLine(ComplexLine().makeLine(start, end));
}

void LineMeshGroup::prepare(){
    mesh = gl::VboMesh::create(mods);
    batch = gl::Batch::create(mesh, renderShader);
    
    isPrepared = true;
}

void LineMeshGroup::addLine(ComplexLine line){
    mods.append(line);
}

void LineMeshGroup::draw(){
    if(isPrepared){
        batch->draw();
    }
}

void LineMeshGroup::test(geom::Source obj){
    mods.append(obj);
}