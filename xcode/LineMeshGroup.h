//
//  LineMeshGroup.h
//  MeshDrawingTests
//  Is able to draw a cluster of lines efficently using geom::Source objects combined
//  with a Batch object
//  Created by Joseph Chow on 8/27/15.
//
//

#ifndef __MeshDrawingTests__LineMeshGroup__
#define __MeshDrawingTests__LineMeshGroup__

#include "cinder/gl/VboMesh.h"
#include "cinder/GeomIo.h"
#include "ComplexLine.h"

class LineMeshGroup {

    //! mesh to draw eveyrthing
    ci::gl::VboMeshRef mesh;
    
    //! collection of all the geom::Source lines
    ci::geom::SourceMods mods;
    
    //! batcher to draw everything
    ci::gl::BatchRef batch;
    
    //! rendering shader
    ci::gl::GlslProgRef renderShader;
    
    //! indicates whether or not the Batch object was initialized.
    bool isPrepared = false;
public:
    LineMeshGroup();
    
    void addLine(ci::vec3 start,ci::vec3 end);
    void addLine(ComplexLine line);
    void test(geom::Source obj);
    
    void prepare();
    
    void draw();
    
};

#endif /* defined(__MeshDrawingTests__LineMeshGroup__) */
