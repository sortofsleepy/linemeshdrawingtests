//
//  ComplexLine.h
//  MeshDrawingTests
//
//  Created by Joseph Chow on 8/28/15.
//
//

#ifndef __MeshDrawingTests__ComplexLine__
#define __MeshDrawingTests__ComplexLine__

#include "cinder/GeomIo.h"
#include "cinder/Vector.h"
#include "MathUtils.h"
#include "cinder/Rand.h"
using namespace ci;
using namespace geom;

class ComplexLine : public Source {
    
    //! stroke width of the line
    int lineWidth = 1;
    
    //! line color
    ci::vec4 color;
    
    //default drawing style
    geom::Primitive drawingStyle;
    
    //! positions for the line
    std::vector<vec3> positions;
public:
    ComplexLine();
    
    //! sets the stroke of the line
    ComplexLine& setLineWidth(int width);
    
    //! sets the color for the line
    ComplexLine& setColor(ci::vec4 color);
    
    //! creates a basic line with a start point and a end point
    ComplexLine& makeLine(ci::vec3 start, ci::vec3 end);
    
    //! creates a line using a BSpline
    ComplexLine& makeLine(const ci::BSpline3f spline, int subdivisions );
    
    //! sets the drawing style of the line
    ComplexLine& setDrawingStyle(geom::Primitive drawingStyle);
    
    ComplexLine&		subdivisions( int subdivs );
    
    size_t		getNumVertices() const override;
    size_t		getNumIndices() const override { return 0; }
    Primitive	getPrimitive() const override { return drawingStyle;}
    uint8_t		getAttribDims( Attrib attr ) const override;
    AttribSet	getAvailableAttribs() const override;
    void		loadInto( Target *target, const AttribSet &requestedAttribs ) const override;
    ComplexLine*		clone() const override { return new ComplexLine( *this ); }
    
    
    
private:
    
    
    int			mRequestedSubdivisions, mNumSubdivisions;
    size_t		mNumVertices;
};


#endif /* defined(__MeshDrawingTests__ComplexLine__) */
