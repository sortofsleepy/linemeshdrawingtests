# Line drawing tests #

This is just a test of how to 

a. draw complex lines

and 

b. how to draw a large number of complex lines efficiently 

this is using [Cinder](http://libcinder.org) as a OpenGL Framework

### How do I get set up? ###
Disclaimer : this is all assuming you're on OSX

* make sure built Cinder source is in a folder called "Cinder" within your documents directory
* place this source within another folder in documents called "apps" 

Of course, if you already know how to change the source lookup in xcode, by all means feel free to move to a location that is most logical for you. 