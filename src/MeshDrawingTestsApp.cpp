#include "cinder/app/App.h"
#include "cinder/app/RendererGl.h"
#include "cinder/gl/gl.h"
#include "LineMesh.h"
#include "ComplexLine.h"
#include "cinder/Rand.h"
#include "TestMesh.h"
using namespace ci;
using namespace ci::app;
using namespace std;

class MeshDrawingTestsApp : public App {
  public:
	void setup() override;
	void mouseDown( MouseEvent event ) override;
	void update() override;
	void draw() override;
    	CameraPersp			mCam;
    gl::VboMeshRef mesh;
    gl::BatchRef batch;
    gl::GlslProgRef renderShader;
    
    TestMesh cube;
    
    geom::SourceMods mods;
    
};

void MeshDrawingTestsApp::setup()
{
    mods.append(geom::Cube().size(0.5, 0.5, 0.5));
    renderShader = gl::getStockShader(gl::ShaderDef().color());
    batch = gl::Batch::create(mods, renderShader);
    
}

void MeshDrawingTestsApp::mouseDown( MouseEvent event )
{
}

void MeshDrawingTestsApp::update()
{
}

void MeshDrawingTestsApp::draw()
{
	gl::clear( Color( 0, 0, 0 ) );
    gl::setMatrices( mCam );
    gl::color(255, 0, 0);
    batch->draw();
   // cube.draw();
}

CINDER_APP( MeshDrawingTestsApp, RendererGl )
